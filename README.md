# SwiftUI-Git



## 概要

XCodeで作成したSwiftUIのプロジェクトをGitLabと連携してみたテストプロジェクトです。

## XCodeとGitLabの連携

1. GitLabのアカウント設定を開く
2. 「アクセストークン」を開く
3. 「新しいトークンを追加」より、トークンを作成(権限は全てチェックを入れる)
4. XCodeを開く
5. 「XCode」　→　「Setting」 → 「Account」 → 「+」 → 「GitLab.com」 → ログインに使用するメールアドレスと3で発行したアクセストークンを入力

## プロジェクトの作成方法

1. GitLabでプロジェクトを作成する
2. XCodeで「Integrate」→「New Git Repository」を選択
3. Create
4. SourceControll(左サイドバーの左から2個目のタブ) → 何もないところで右クリック → 「Add New Exsisting Repository」 → GitLabのプロジェクトのURL(HTTPSクローンにて発行したURL)を貼り付けて「Add」
5. とりあえずPullする(README.mdがプルされる)
6. Pushする(XCodeプロジェクトがプッシュされる)

これにて、Gitとの連携は完了する


## プロジェクトをクローンする

1. XCodeを開く
2. 「Clone Git Repository」を選択
3. プロジェクトを選択
4. ローカルの保存先を選択し、「Clone」
