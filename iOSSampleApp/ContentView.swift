//
//  ContentView.swift
//  iOSSampleApp
//
//  Created by YasudaShunsuke on 2024/06/17.
//

import SwiftUI

struct ContentView: View {
    @State var str = "Hello SwiftUI"
    
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text(str)
                .foregroundColor(Color.red)
            Button("ボタン"){
                str = "ボタンが押されたよ"
            }
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
