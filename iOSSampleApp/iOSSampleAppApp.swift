//
//  iOSSampleAppApp.swift
//  iOSSampleApp
//
//  Created by YasudaShunsuke on 2024/06/17.
//

import SwiftUI

@main
struct iOSSampleAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
